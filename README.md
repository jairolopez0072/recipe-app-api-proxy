# Recipe App Api Proxy

Nginx proxy app for our recipe api

## Usage

### Environments Variables


* `LISTEN_PORT` - Port to listen on (default : `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default : `app`)
* `APP_PORT` - Hostname of the app to forward request to (default : `9000`)

